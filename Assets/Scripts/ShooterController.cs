
using UnityEngine;
using Cinemachine;
using StarterAssets;

public class ShooterController : MonoBehaviour
{
	[SerializeField]
	[Tooltip("Camera used for aiming down sights")]
	private CinemachineVirtualCamera _aimCamera;

	[SerializeField]
	[Tooltip("Look sensitivity")]
	[Range(0.0f, 1.0f)]
	private float _lookSensitivity = 1.0f;

	[SerializeField]
	[Tooltip("Aim sensitivity")]
	[Range(0.0f, 1.0f)]
	private float _aimSensitivity = 0.5f;

	[SerializeField]
	[Tooltip("Mask defining all the layers weapons will collide with")]
	private LayerMask _weaponColliderMask = new LayerMask();

	[SerializeField]
	private BulletProjectile _pfBullet;

	[SerializeField]
	private Transform _firePoint;

	private ThirdPersonController _tpscontroller;
	private StarterAssetsInputs _inputs;

	private void Awake()
	{
		_inputs = GetComponent<StarterAssetsInputs>();
		_tpscontroller = GetComponent<ThirdPersonController>();
	}

	private void Update()
	{
		Vector2 center = new Vector2(Screen.width / 2, Screen.height / 2);
		Ray aimRay = Camera.main.ScreenPointToRay(center);
		Vector3 targetPosition = Vector3.zero;
		
		if (Physics.Raycast(aimRay, out RaycastHit hit, 999f, _weaponColliderMask))
			targetPosition = hit.point;
		else
			targetPosition = aimRay.GetPoint(999);

		if (_inputs.aim)
		{
			_aimCamera.gameObject.SetActive(true);
			_tpscontroller.SetSensitivity(_aimSensitivity);
			_tpscontroller.SetRotateOnMove(false);

			Vector3 targetPositionXZ = targetPosition;
			targetPositionXZ.y = transform.position.y;
			Vector3 aimDirection = (targetPositionXZ - transform.position).normalized;

			transform.forward = Vector3.Lerp(transform.forward, aimDirection, Time.deltaTime * 20.0f);
		}
		else
		{
			_aimCamera.gameObject.SetActive(false);
			_tpscontroller.SetSensitivity(_lookSensitivity);
			_tpscontroller.SetRotateOnMove(true);
		}

		if (_inputs.attack)
		{
			Vector3 aimDir = (targetPosition - _firePoint.position).normalized;
			Instantiate(_pfBullet, _firePoint.position, Quaternion.LookRotation(aimDir, Vector3.up));
			_inputs.attack = false;
		}
	}   
}
