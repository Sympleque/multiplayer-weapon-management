# Multiplayer Weapon Management

Unfortunately, architecture for multiplayer games is far different than it is for singleplayer games. Weapon management is not as easy. This project seeks to experiement with different weapon management architectures that are scalable and work well with multiplayer games. This sample project includes different damage types (melee, single target, splash) as well as weapon switching.
